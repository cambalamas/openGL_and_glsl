#version 330 core

out vec4 outColor;
uniform sampler2D colorTex;

in vec3 norm;

void main()
{
	outColor = norm.xyzz;
}
